NAME = ft_nmap

FLAG = -Wall -Wextra -Werror -D_GNU_SOURCE -g -o3

OS_NAME := $(shell uname)
ifeq ($(OS_NAME),Darwin)
	FLAG += -D __MAC__
endif

HEADER = includes/ft_nmap.h includes/utils.h includes/inet.h includes/config.h includes/spec.h includes/ft_ping.h includes/icmp.h
INCLUDES = -I includes/

SRCS_PATH = srcs/
UTILS_PATH = utils/
PING_PATH = ping/
SRCS_NAME = debug.c main.c parsing.c ft_nmap.c thread_exec.c checksum.c threads_manager.c \
			network_utils.c get_time_now.c make_ping.c
UTILS_NAME = ft_strcmp.c ft_atoi.c ft_isalpha.c ft_isdigit.c ft_isblank.c ft_memalloc.c \
			ft_memcpy.c str_is_number.c ft_memset.c listlen.c is_str_in_tab.c \
			ft_strsub.c ft_strsplit.c ft_strstr.c ft_strlen.c ft_strdup.c get_next_line.c \
			ft_strjoin.c ft_strnew.c ft_strdel.c ft_strchr.c ft_memchr.c ft_strcpy.c \
			ft_strcat.c ft_strncmp.c ft_itoa.c ft_memdel.c ft_timeval_sub.c
PING_NAME = array_len.c debug_ping.c ft_strtrim.c packet.c tab_2d_len.c utils.c check_packet.c \
			ft_count_words.c ft_ping.c main_ping.c ptr2Ddel.c time.c

UTILS = $(addprefix $(UTILS_PATH), $(UTILS_NAME))
PING = $(addprefix $(PING_PATH), $(PING_NAME))
SRCS_NAME += $(UTILS)
SRCS_NAME += $(PING)
SRCS = $(addprefix $(SRCS_PATH),$(SRCS_NAME))

OBJS_PATH = .objs/
OBJS_NAME = $(SRCS_NAME:.c=.o)
OBJS = $(addprefix $(OBJS_PATH),$(OBJS_NAME))

all: $(NAME)

$(NAME): $(OBJS)
	$(CC) $(FLAG) $(INCLUDES) -o $(NAME) $(OBJS)

$(OBJS_PATH)%.o:$(SRCS_PATH)%.c $(HEADER)
	mkdir -p $(OBJS_PATH)
	mkdir -p $(addprefix $(OBJS_PATH), $(UTILS_PATH))
	mkdir -p $(addprefix $(OBJS_PATH), $(PING_PATH))
	$(CC) $(FLAG) $(INCLUDES) -o $@ -c $<

clean:
	$(RM) -rf $(OBJS_PATH)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
