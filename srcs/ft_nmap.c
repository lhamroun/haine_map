#include "ft_nmap.h"

static int count_nmap_size(t_nmap *nmap)
{
	int i = 0;

	while (nmap[i].address != NULL)
		i++;
	return i;
}

static int find_next_waiting_index(t_nmap *nmap, size_t len)
{
	size_t  i = 0;
	int     ret = -1;

	while (i < len)
	{
		if (nmap[i].address && nmap[i].status == WAITING)
			return (int)i;
		else if (nmap[i].address &&(nmap[i].status == PREPARED || nmap[i].status == BUSY) && nmap[i].status == AVAILABLE)
			ret = -2;
		i++;
	}
	return ret;
}

static t_nmap copy_nmap_item(t_nmap item)
{
    t_nmap new;

    new.verbose = item.verbose;
    new.status = item.status;
    new.address = item.address;
    new.dns = item.dns;
    new.dport = item.dport;
    new.sport = item.sport;
    new.response = item.response;
    new.index = item.index;
    new.scan_type = item.scan_type;
    return new;
}

int		get_nb_scan(enum e_scan scan_type)
{
	int		nb_scan = 0;

	if (scan_type == ALL)
		return 6;
	if ((scan_type & SYN))
		nb_scan += 1;
	if ((scan_type & NUL))
		nb_scan += 1;
	if ((scan_type & ACK))
		nb_scan += 1;
	if ((scan_type & FIN))
		nb_scan += 1;
	if ((scan_type & UDP))
		nb_scan += 1;
	if ((scan_type & XMAS))
		nb_scan += 1;
	return nb_scan;
}

enum e_scan	get_scan_type_from_index(int k, enum e_scan input_scan_type)
{
	int			cpt = 0;
	uint8_t		tmp = 0;

	if (input_scan_type == ALL)
		input_scan_type = (SYN | UDP | XMAS | FIN | NUL | ACK);
	for (int i = 0; i < 6; i++)
	{
		tmp = input_scan_type >> i;
		tmp = tmp & 0x01;
		if (cpt == k && tmp == 1)
		{
			return (1 << i);
		}
		else if (tmp == 1)
			cpt++;
	}
	return SCAN_ERROR;
}

struct addrinfo *get_addrinfo_from_str(char *addr)
{
	struct addrinfo		hints;
    struct addrinfo		*server;

	ft_memset(&hints, 0, sizeof(struct addrinfo));
    server = NULL;
	hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_RAW;
    hints.ai_protocol = IPPROTO_UDP;
    hints.ai_flags = AI_CANONNAME;
	if (getaddrinfo(addr, NULL, &hints, &server) != 0)
		return NULL;
    return server;
}

static char *resolve_dns_addr(char *ip)
{
	struct addrinfo *serv;
	char            *host_buf;

	host_buf = ft_memalloc(NI_MAXHOST);
	if (!host_buf)
		return ft_memalloc(1);
	serv = get_addrinfo_from_str(ip);
	if (!serv || !serv->ai_addr)
	{
		ft_memdel((void **)&host_buf);
		return ft_memalloc(1);
	}
	getnameinfo(serv->ai_addr, INET_ADDRSTRLEN, host_buf, NI_MAXHOST, NULL, 0, NI_NAMEREQD);
	freeaddrinfo(serv);
	return host_buf;
}

static t_nmap *get_main_structure(t_params *params)
{
	int		cpt;
	t_nmap	*nmap;
	int		nb_scan;
	ssize_t	i;

	nb_scan = get_nb_scan(params->scan_type);
	cpt = 0;
	if (!(nmap = (t_nmap*)ft_memalloc(sizeof(t_nmap) * (params->ports_size * listlen(params->ip) * nb_scan + 1))))
		return NULL;
	for (i = 0; params->ip[i]; i++)
	{
        for (size_t j = 0; j < params->ports_size; j++)
        {
            for (int k = 0; k < nb_scan; k++)
            {
				nmap[cpt].verbose = params->verbose;
				nmap[cpt].address = ft_strdup(params->ip[i]);
				nmap[cpt].dns = resolve_dns_addr(params->ip[i]);
				nmap[cpt].dport = params->ports[j];
				nmap[cpt].sport = 12345 + (k * (SCAN_MAX_RETRIES + 1) + j);
				nmap[cpt].status = WAITING;
				nmap[cpt].index = cpt;
				nmap[cpt].response = (t_response *)ft_memalloc(sizeof(t_response));
				if (!nmap[cpt].address)
				{
					ft_memdel((void **)&nmap);
					return NULL;
				}
				if (!nmap[cpt].response)
				{
					for (int e = 0; e < cpt; e++)
					{
						ft_memdel((void **)&nmap->response);
					}
					ft_memdel((void **)&nmap);
					return NULL;
				}
				nmap[cpt].response->service = NULL;
				nmap[cpt].scan_type = get_scan_type_from_index(k, params->scan_type);
				//debug_nmap(nmap[cpt]);
				++cpt;
			}
		}
	}
	nmap[params->ports_size * listlen(params->ip) * nb_scan].address = NULL;
	return nmap;
}

t_nmap *process(t_params *params)
{
	int				nb_job;
	int				nb_job_done;
    int             error_count;
	t_nmap			*nmap;
	t_nmap			*done;
	pthread_t		*threads;

	nb_job_done = 0;
    error_count = 0;
	threads = NULL;

    nmap = get_main_structure(params);
	if (!nmap)
		return NULL;
	nb_job = count_nmap_size(nmap);

	done = ft_memalloc(sizeof(t_nmap) * (nb_job + 1));

	if (!done)
	{
		for (int i = 0; i < nb_job; i++)
		{
			ft_memdel((void **)&nmap[i].response);
		}
		ft_memdel((void **)&nmap);
		return NULL;
	}
	done[nb_job].address = NULL;
	if (params->speedup > 0)
	{
		threads = ft_memalloc(sizeof(pthread_t) * params->speedup);
		if (!threads)
		{
			ft_memdel((void **)&done);
			for (int i = 0; i < nb_job; i++)
			{
				ft_memdel((void **)&nmap[i].response);
			}
			ft_memdel((void **)&nmap);
			return NULL;
		}
	}

	if (params->speedup > 0 && !init_threads(nb_job, params, threads, nmap))
	{
		ft_memdel((void **)&done);
		ft_memdel((void **)&threads);
		for (int i = 0; nmap[i].response; i++)
		{
			ft_memdel((void **)&nmap[i].address);
		}
		for (int i = 0; nmap[i].response; i++)
		{
			ft_memdel((void **)&nmap[i].response);
		}
		ft_memdel((void **)&nmap);
		return NULL;
	}

    if (params->speedup > 0)
	{
        while (nb_job_done < nb_job)
		{
            for (int j = 0; j < nb_job; j++)
			{
                if (nmap[j].status == ERROR && nmap[j].address)
                {
                    error_count++;
                    if (error_count >= params->speedup / 10)
                    {
                        dprintf(STDERR_FILENO, "Too many errors\n");
                        goto end_loop;
                    }
                    nmap[j].status = WAITING;
                }
                if (nmap[j].status == AVAILABLE && nmap[j].address)
				{
                    int k;
                    nb_job_done++;
                    done[nmap[j].index] = copy_nmap_item(nmap[j]);
                    if (nb_job_done == nb_job)
                        nmap[j].address = NULL;
                    k = find_next_waiting_index(nmap, nb_job);
                    if (k == -1 && nb_job_done == nb_job)
                    {
                        goto end_loop;
                    }
                    else if (k >= 0)
					{
                        nmap[k].status = PREPARED;
                        nmap[j] = nmap[k];
                        nmap[k].address = NULL;
                    }
					else
                        nmap[j].address = NULL;
                }
            }
        }

    } else {
        for (int j = 0; j < nb_job; j++) {
            nmap[j].status = PREPARED;
            scan_port(&nmap[j]);
            if (nmap[j].status == ERROR)
            {
                perror("error");
                dprintf(STDERR_FILENO, "QUITTING\n");
                return NULL;
            }
            else
                done[nmap[j].index] = copy_nmap_item(nmap[j]);
        }
    }
	end_loop:

	if (params->speedup > 0 && !join_threads(params->speedup, nb_job_done, threads)) // leaks possible
	{
		ft_memdel((void **)&done);
		ft_memdel((void **)&threads);
		for (int i = 0; nmap[i].address; i++)
		{
			ft_memdel((void **)&nmap[i].address);
		}
		for (int i = 0; nmap[i].response; i++)
		{
			ft_memdel((void **)&nmap[i].response);
		}
		ft_memdel((void **)&nmap);
		return NULL;
	}
	ft_memdel((void **)&threads);
	ft_memdel((void**)&nmap);
	return done;
}
