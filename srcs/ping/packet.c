#include <ft_ping.h>

extern t_ping *g_ping;

void	print_not_received_pkt(int duration)
{
	if (!g_ping->args.quiet)
		printf("Request timeout for icmp_seq %u\n", g_ping->nbPkt - 1);
	g_ping->pktLost++;
	ft_usleep(duration * 1000000);
}

unsigned short	checksum_ping(void *b, int len)
{
	unsigned short	*buf;
	unsigned int	sum;
	unsigned short	result;

	buf = b;
	sum = 0;
	for (sum = 0; len > 1; len -= 2)
		sum += *buf++;
	if (len == 1)
		sum += *(unsigned char*)buf;
	sum = (sum >> 16) + (sum & 0xFFFF);
	sum += (sum >> 16);
	result = ~sum;
	return result;
}

void	*codePingPacket(void)
{
	void			*ptr;
	char			*msg;
	struct icmphdr	*icmp_pkt;

	if (!(ptr = (char *)ft_memalloc(PKT_SIZE + 1)))
		return NULL;
	icmp_pkt = (struct icmphdr *)ptr;
	msg = (char *)ptr + sizeof(struct icmphdr);
	for (unsigned long i = 0; i < PKT_SIZE - sizeof(struct icmphdr) - 1; i++)
		msg[i] = i + '0';
	msg[PKT_SIZE - sizeof(struct icmphdr)] = '\0';
	icmp_pkt->type = ICMP_ECHO;
	icmp_pkt->code = 0;
#ifdef __MAC__
	icmp_pkt->id = getpid();
	icmp_pkt->sequence = g_ping->nbPkt;
#else
	icmp_pkt->un.echo.id = getpid();
	icmp_pkt->un.echo.sequence = g_ping->nbPkt;
#endif
	icmp_pkt->checksum = 0;
	icmp_pkt->checksum = checksum_ping(icmp_pkt, sizeof(t_pkt));
	return ptr;
}

t_bool	sendPkt(void)
{
	ssize_t				res = 0;
	t_pkt				*pkt;
	struct sockaddr		server;

	ft_memset(&server, 0, sizeof(struct sockaddr_in));
	server = *g_ping->serverInfo->ai_addr;
	if (!(pkt = (t_pkt *)codePingPacket()))
		return FALSE;
	res = sendto(	g_ping->fd_socket,
					(const void *)pkt,
					PKT_SIZE,
					0,
					&server,
					sizeof(server));
	if (res != -1)
		g_ping->sending = TRUE;
	g_ping->nbPkt++;
	gettimeofday(&g_ping->rtt.tv, 0);;
	free(pkt);
	return res == -1 ? FALSE : TRUE;
}

struct msghdr	set_msghdr(void *pkt)
{
	struct msghdr	msg;
	struct iovec	*iov;

	(void)pkt;
	ft_memset(&msg, 0, sizeof(struct msghdr));
	iov = ft_memalloc(sizeof(struct iovec));
	msg.msg_name = ft_memalloc(sizeof(struct sockaddr_in));
	msg.msg_namelen = sizeof(struct sockaddr_in);
	iov->iov_base = pkt;
	iov->iov_len = PKT_SIZE + sizeof(struct iphdr);
	msg.msg_iov = iov;
	msg.msg_iovlen = 1;
	msg.msg_control = ft_memalloc(512);
	msg.msg_controllen = 512;
	return msg;
}

t_bool	decodePingPacket(char **pkt)
{
	int				res = 0;
	struct iphdr	*ip;
	struct icmphdr	*icmp;

	icmp = (struct icmphdr *)*pkt;
	ip = (struct iphdr *)((char *)icmp - sizeof(struct iphdr));
	//icmp = align_packet(icmp); // not necessary now

	res = check_iphdr(*ip);
	res += check_icmphdr(*icmp);
	g_ping->ttl = ip->ttl;
	if (res)
	{
		if (g_ping->args.verbose)
		{
			printf("icmp_seq %u received but corrupted\n", g_ping->nbPkt - 1);
			printf("received packet\n");
			print_ip(*ip);
			print_icmp(*icmp);
		}
		return FALSE;
	}
	return TRUE;
}

void	recvPkt(void)
{
	int				i = 0;
	ssize_t			res = 0;
	struct msghdr	msg;
	char			*pkt;
	unsigned long	start;

	start = get_time_now();
	while (get_time_now() - start < 1000000)
	{
		pkt = ft_memalloc(PKT_SIZE + sizeof(struct iphdr));
		msg = set_msghdr(pkt);
		res = recvmsg(g_ping->fd_socket, &msg, 0);
		free(msg.msg_name);
		free(msg.msg_control);
		g_ping->sending = FALSE;
		pkt = ((char *)msg.msg_iov->iov_base + sizeof(struct iphdr));
		if (res == PKT_SIZE + sizeof(struct iphdr))
		{
			if (decodePingPacket(&pkt))
			{
				g_ping->rtt = update_rtt_stat();
				if (g_ping->args.ttl_max != -1 && g_ping->args.ttl_max < (int)g_ping->ttl)
				{
					print_not_received_pkt(i);
					goto skip;
				}
				if (!g_ping->args.quiet)
				{
					printf("%u bytes from %s: icmp_seq=%u ttl=%u time=%.3f ms\n",
						g_ping->packetSize,
						g_ping->host,
						g_ping->nbPkt - 1,
						g_ping->ttl,
						g_ping->rtt.curr);
				}
				skip:;
				free(msg.msg_iov->iov_base);
				free(msg.msg_iov);
				ft_usleep(g_ping->args.rtt * 1000000 - (unsigned int)(g_ping->rtt.curr * 1000));
				return;
			}
		}
	}
	i = g_ping->args.rtt;
	free(msg.msg_iov->iov_base);
	free(msg.msg_iov);
	print_not_received_pkt(0);
}

