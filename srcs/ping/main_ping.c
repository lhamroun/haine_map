#include "ft_ping.h"

/*
	__attribute__((destructor)) void loop(void){while(1);}
//*/

void	usage_ping(int ret)
{
	printf("usage: ./ft_ping [-v] [-h] [-q] [-n [int]] [-t [int]] [-c [int]] <ipv4>\n	-v: verbose\n	-h: help\n	-q: quiet\n	-n: ttl max (0 < x < 256)\n	-t: timeout (0 < y < INT_MAX)\n	-c: remaining packets (0 < z < INT_MAX)\n");
	exit(ret);
}

t_bool	is_valid_number(char *str)
{
	int		i = 0;

	while (str[i])
	{
		if (!ft_isdigit(str[i]))
			return FALSE;
		i++;
	}
	return TRUE;
}

t_bool	is_valid_ip(char *ip)
{
	int		num = 0;
	int		dots = 0;
	size_t	cpt = 0;
	char	*ptr;
	char	*tmp;

	if (ip == NULL)
		return FALSE;
	ptr = ft_strdup(ip);
	tmp = ptr;
	if (!ptr)
		return FALSE;
	ptr = strtok(ptr, ".");
	for (size_t i = 0; i < ft_strlen(ip); i++)
	{
		if (ft_isdigit(ip[i]))
			cpt++;
	}
	if (cpt == ft_strlen(ip))
	{
		ft_strdel(&tmp);
		return FALSE;
	}
	if (ft_isalpha(ip[0]))
	{
		ft_strdel(&tmp);
		return TRUE;
	}
	while (ptr)
	{
		if (ft_isalpha(*ptr))
			break ;
		else if (!is_valid_number(ptr))
		{
			ft_strdel(&tmp);
			return FALSE;
		}
		num = ft_atoi(ptr);
		if (num >= 0 && num <= 255)
		{
			ptr = strtok(NULL, ".");
			if (ptr != NULL)
				dots++;
		}
		else
		{
			printf("ping: %s: Name or service not known\n", ip);
			ft_strdel(&tmp);
			return FALSE;
		}
	}
	ft_strdel(&tmp);
	return TRUE;
	return dots != 3 ? FALSE : TRUE;
}

t_bool	check_ip(char *addr)
{
	if (!ft_isalpha(addr[0]) && !ft_isdigit(addr[0]))
		return FALSE;
	if (!is_valid_ip(addr))
		return FALSE;
	return TRUE;
}
