#include "ft_ping.h"

t_ping	*g_ping = NULL;

void	intQuitHandler(int sig)
{
	(void)sig;
	if (g_ping->sending == TRUE)
		g_ping->pktLost++;
	printf("--- %s ping statistics ---\n", g_ping->args.addr);
	printf("%u packets trasmitted, %u packets received, %.1f%% packet loss\n",
		g_ping->nbPkt,
		g_ping->nbPkt - g_ping->pktLost,
		g_ping->nbPkt == 0 ? 0.0 : (float)(100 * g_ping->pktLost / g_ping->nbPkt));
	if (g_ping->nbPkt > g_ping->pktLost)
		printf("round-trip min/avg/max/stddev = %.3f/%.3f/%.3f/%.3f ms\n",
			g_ping->rtt.min == 1000.0 ? 0.0 : g_ping->rtt.min,
			g_ping->rtt.average,
			g_ping->rtt.max,
			g_ping->rtt.stddev);
	clean_memory();
}

void	intStatHandler(int sig)
{
	(void)sig;
	printf("%u/%u packets, %uloss, min/avg/ewma/max = %.3f/%.3f/%.3f/%.3f ms\n", g_ping->nbPkt - g_ping->pktLost, g_ping->nbPkt, g_ping->pktLost, g_ping->rtt.min == 1000.0 ? 0.0 : g_ping->rtt.min, g_ping->rtt.average, g_ping->rtt.stddev, g_ping->rtt.max);
}

char	*setHostIp(struct addrinfo *res)
{
	char				*addr;
	struct sockaddr_in	*tmp;

	if (!(addr = ft_memalloc(sizeof(char) * INET_ADDRSTRLEN)))
		return NULL;
	tmp = (struct sockaddr_in *)res->ai_addr;
	if (!inet_ntop(res->ai_family, &tmp->sin_addr, addr, INET_ADDRSTRLEN))
		return NULL;
	return addr;
}

struct addrinfo	*setServerInfo(char *addr)
{
	struct addrinfo		*server;
	struct addrinfo		hints;

	ft_memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = PF_UNSPEC;
	if (getaddrinfo(addr, NULL, &hints, &server) != 0)
	{
		return NULL;
	}
	/*
	while (server->ai_next)
	{
		if (server->ai_family == 2 && server->ai_socktype == 3)
			break ;
		server = server->ai_next;
	}
	*/
	ft_memset(((struct sockaddr_in *)server->ai_addr)->sin_zero, 0, sizeof(((struct sockaddr_in *)server->ai_addr)->sin_zero));
	return server;
}

int		get_socket(struct timeval time, int ttl)
{
	int				fd_socket;
	int				res = 0;

	fd_socket = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	if (fd_socket == -1)
		return -1;
	res = setsockopt(fd_socket, IPPROTO_IP, IP_TTL, &ttl, sizeof(int));
	if (res != 0)
		return -1;
	res = setsockopt(fd_socket, SOL_SOCKET, SO_RCVTIMEO, &time, sizeof(struct timeval));
	if (res != 0)
		return -1;
	return fd_socket;
}

t_ping	*setPingEnv(t_args args)
{
	t_ping	*ping;

	if (!(ping = ft_memalloc(sizeof(t_ping))))
		return NULL;
	ping->args = args;
	ping->packetSize = PKT_SIZE;
	ping->rtt.min = 1000.0;
	if (ping->args.quiet)
		ping->args.verbose = 0;
	if (ping->args.rtt == -1)
		ping->args.rtt = RTT;
	if (ping->args.ttl_max != -1)
		ping->ttl = TTL;
	else
		ping->ttl = ping->args.ttl_max;;

	if ((ping->fd_socket = get_socket((struct timeval){(time_t)ping->args.rtt, 0}, ping->ttl)) < 0)
	{
		return NULL;
	}
	if (!(ping->serverInfo = setServerInfo(args.addr)))
	{
		return NULL;
	}
	if (!(ping->host = setHostIp(ping->serverInfo)))
	{
		return NULL;
	}
	ping->host_ul = ((struct sockaddr_in *)ping->serverInfo->ai_addr)->sin_addr.s_addr;
	return ping;
}

int		ft_ping(t_args args)
{
	g_ping = setPingEnv(args);
	if (!g_ping)
	{
		printf("ping: cannot resolve %s: Unknown host\n", args.addr);
		return -1;
	}
	printf("PING %s (%s): %u data bytes\n",
		g_ping->args.addr,
		g_ping->host,
		g_ping->packetSize - (unsigned int)(sizeof(struct icmphdr)));
	gettimeofday(&g_ping->rtt.tv, NULL);
	g_ping->startTime = g_ping->rtt.tv;
	while (g_ping->args.count == -1 || g_ping->args.count > 0)
	{
		if (sendPkt())
			recvPkt();
		else
		{
			if (g_ping->args.verbose)
				printf("fail to send icmp_seq %u\n", g_ping->nbPkt - 1);
			print_not_received_pkt(1);
		}
		if (g_ping->args.count != -1)
			g_ping->args.count--;
	}
	intQuitHandler(SIGINT);
	printf("\n");
	//freeaddrinfo(g_ping->serverInfo);
	return g_ping->pktLost;
}

