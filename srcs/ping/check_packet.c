#include "ft_ping.h"

extern t_ping	*g_ping;

int		check_iphdr(struct iphdr ip)
{
	if (ip.version != IPV4)
		return 1;
	if (ip.ihl != 5)
		return 2;
	if (ip.ttl == 0)
		return 3;
	if (ip.protocol != IPPROTO_ICMP)
		return 4;
	if (ip.saddr != g_ping->host_ul)
		return 5;
	return 0;
}

int		check_icmphdr(struct icmphdr icmp)
{
	if (icmp.type != ICMP_ECHOREPLY)
		return 10;
	if (icmp.code != ICMP_ECHOREPLY)
		return 11;
#ifdef __MAC__
	if (icmp.sequence != g_ping->nbPkt - 1)
		return 12;
	if (icmp.id != (u_int16_t)getpid())
		return 13;
#else
	if (icmp.un.echo.sequence != g_ping->nbPkt - 1)
		return 12;
	if (icmp.un.echo.id != (u_int16_t)getpid())
		return 13;
#endif
	return 0;
}

struct icmphdr	*align_packet(struct icmphdr *icmp)
{
	unsigned long	i = 1;
	unsigned long	len = 256;
	void			*ptr = icmp;
	char			pkt_msg[] = "0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdef";

	if (ft_strcmp(((t_pkt *)icmp)->msg, pkt_msg))
	{
		while (i <= len)
		{
			ptr = (char *)icmp - i;
			if (!ft_strcmp(((t_pkt *)ptr)->msg, pkt_msg))
			{
				icmp = (struct icmphdr *)ptr;
				goto end;
				break ;
			}
			++i;
		}
		i = 1;
		ptr = icmp;
		while (i <= len)
		{
			ptr = (char *)icmp + i;
			if (!ft_strcmp(((t_pkt *)ptr)->msg, pkt_msg))
			{
				icmp = (struct icmphdr *)ptr;
				goto end;
				break ;
			}
			++i;
		}
	}
	end:
	return icmp;
}
