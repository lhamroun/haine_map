#include "ft_nmap.h"

void    debug_network_address(uint32_t ip)
{
    printf("network @	: %u.%u.%u.%u\n",
        ( 0x000000ff & ip),
        ((0x0000ff00 & ip) >> 8),
        ((0x00ff0000 & ip) >> 16),
        ((0xff000000 & ip) >> 24));
}

void    debug_tcp(struct tcphdr *tcp)
{
    printf("TCP packet contains :\n");
    printf("sport       : %hu\n", ntohs(tcp->th_sport));
    printf("dport       : %hu\n", ntohs(tcp->th_dport));
    //printf("sport       : %hu\n", tcp->th_sport);
    //printf("dport       : %hu\n", tcp->th_dport);
    printf("seq         : %u\n", tcp->th_seq);
    printf("ack         : %u\n", tcp->th_ack);
    printf("offset      : %d\n", tcp->th_off);
    printf("flags       : CWR - ECE - URG - ACK - PUSH - RST - SYN - FIN\n");
    printf("flags       : ");
	printf(" %d  -  %d  -  %d  -  %d  -  %d   -  %d  -  %d  -  %d\n", (128 & tcp->th_flags) >> 7,
		(64 & tcp->th_flags) >> 6,
		(32 & tcp->th_flags) >> 5,
		(16 & tcp->th_flags) >> 4,
		(8 & tcp->th_flags) >> 3,
		(4 & tcp->th_flags) >> 2,
		(2 & tcp->th_flags) >> 1,
		(1 & tcp->th_flags));
    printf("win         : %hu\n", tcp->th_win);
    printf("chksum      : 0xh%x\n", tcp->th_sum);
    printf("win         : %hu\n", tcp->th_urp);
    printf("\n\n");
}

void    debug_udp(struct udphdr *udp)
{
    printf("UDP packet contains :\n");
    printf("sport       : %hu\n", ntohs(udp->uh_sport));
    printf("dport       : %hu\n", ntohs(udp->uh_dport));
    printf("len         : %u\n", ntohs(udp->uh_ulen));
    printf("chksum      : 0x%x\n", udp->uh_sum);
    char *tmp = (char *)udp + sizeof(struct udphdr);
    printf("    data    : %s\n", tmp);
    printf("\n\n");
}

void    debug_icmp(struct icmphdr *icmp)
{
    printf("ICMP packet contains :\n");
    printf("type        : %u\n", icmp->type);
    printf("code        : %u\n", icmp->code);
#ifdef __MAC__
    printf("sequence    : %u\n", icmp->sequence);
#endif
    printf("checksum    : 0x%x\n", icmp->checksum);
#ifdef __MAC__
    printf("id          : %u\n", icmp->id);
#endif
    char *tmp = (char *)icmp + sizeof(struct icmphdr);
    printf("    data    : %s\n", tmp);
    printf("\n\n");
}

void    debug_ip(struct iphdr *ip)
{
    printf("IP packet contains :\n");
    printf("ihl         : %d\n", ip->ihl);
    printf("version     : %d\n", ip->version);
    printf("tos         : %hhu\n", ip->tos);
    printf("tot_len     : %hu\n", ip->tot_len);
    printf("id          : %hu\n", ip->id);
    printf("frag_off    : %hu\n", ip->frag_off);
    printf("ttl         : %hhu\n", ip->ttl);
    printf("protocol    : %s\n", ip->protocol == 1 ? "ICMP" : ip->protocol == 17 ? "UDP" : ip->protocol == 6 ? "TCP" : "OTHER PROTOCOL");
    printf("check       : 0x%hx\n", ip->check);
    printf("saddr       : %u.%u.%u.%u\n",
        ( 0x000000ff & ip->saddr),
        ((0x0000ff00 & ip->saddr) >> 8),
        ((0x00ff0000 & ip->saddr) >> 16),
        ((0xff000000 & ip->saddr) >> 24));
    printf("daddr       : %u.%u.%u.%u\n",
        ( 0x000000ff & ip->daddr),
        ((0x0000ff00 & ip->daddr) >> 8),
        ((0x00ff0000 & ip->daddr) >> 16),
        ((0xff000000 & ip->daddr) >> 24));
    printf("\n\n");
}

void    debug_sockaddr(struct sockaddr_in *addr)
{
    printf("sockaddr_in :\n");
    printf("    sin_family      --> %s\n", addr->sin_family == AF_INET ? "AF_INET" : "OTHER PROTOCOL");
    printf("    sin_port        --> %hu\n", ntohs(addr->sin_port));
    printf("    s_addr          --> %u.%u.%u.%u\n", (addr->sin_addr.s_addr & 0x000000ff), ((addr->sin_addr.s_addr & 0x0000ff00) >> 8), ((addr->sin_addr.s_addr & 0x00ff0000) >> 16), ((addr->sin_addr.s_addr & 0xff000000) >> 24));
    printf("    sin_zero        --> %s\n", addr->sin_zero);
    printf("\n\n");
}

void    debug_addrinfo(struct addrinfo *addr)
{
    printf("addr infos :\n");
    printf("    ai_flags        --> %d\n", addr->ai_flags);
    printf("    ai_family       --> %s\n", addr->ai_family == AF_INET ? "AF_INET" : "OTHER PROTOCOL");
    printf("    ai_socktype     --> %s\n", addr->ai_socktype == SOCK_RAW ? "SOCK_RAW" : addr->ai_socktype == SOCK_DGRAM ? "SOCK_DGRAM" : "OTHER SOCK TYPE");
    printf("    ai_protocole    --> %d\n", addr->ai_protocol);
    printf("    ai_addrlen      --> %d\n", addr->ai_addrlen);
#ifdef __MAC__
    printf("    ai_addr len     --> %d\n", addr->ai_addr->sa_len);
#endif
    printf("    ai_addr fam     --> %d\n", addr->ai_addr->sa_family);
    printf("    ai_addr data    --> %s\n", addr->ai_addr->sa_data);
    printf("    ai_canonname    --> %s\n", addr->ai_canonname);
    printf("    ai_next         --> %p\n", addr->ai_next);
    printf("\n\n");
}

void    debug_options_check(t_options_check opt_check)
{
    printf("options checker :\n");
    printf("option check n_help		%d\n", opt_check.n_helper);
    printf("option check n_verbose	%d\n", opt_check.n_verbose);
    printf("option check n_speedup	%d\n", opt_check.n_speedup);
    printf("option check n_scan_type	%d\n", opt_check.n_scan_type);
    printf("option check n_ip_addr	%d\n", opt_check.n_ip_addr);
    printf("option check n_file		%d\n", opt_check.n_file);
    printf("option check n_port size	%d\n", opt_check.n_ports);
	printf("\n");
}

void    debug_options(t_options opt)
{
	char *tmp = ft_itoa(opt.scan_type);

	printf("options checker :\n");
	printf("option help			%d\n", opt.helper);
	printf("option verbose		%d\n", opt.verbose);
	printf("option speedup		%d\n", opt.speedup);
	printf("option scan_type	%s\n", opt.scan_type == ALL ? "ALL" :
										opt.scan_type == SYN ? "SYN" :
										opt.scan_type == NUL ? "NULL" :
										opt.scan_type == ACK ? "ACK" :
										opt.scan_type == FIN ? "FIN" :
										opt.scan_type == XMAS ? "XMAS" :
										opt.scan_type == UDP ? "UDP" : 
										(opt.scan_type & (SYN | NUL | FIN | UDP | XMAS | ACK)) ? tmp : "ERROR_TYPE_SCAN");
	printf("option ip_addr		%s\n", opt.ip_addr);
	printf("option file		%s\n", opt.file);
	printf("option port size	%u\n", opt.ports_size);
	printf("option port		");
	for (uint32_t i = 0; i < opt.ports_size ; i++)
	{
		if (i == 15)
			printf(" ... ");
		else if (i > 15 && i < opt.ports_size - 4)
			;
		else
			printf("%hu ", opt.ports[i]);
	}
	printf("\n");
	printf("\n");
	free(tmp);
}

void	debug_params(t_params params)
{
	printf("params checker :\n");
	printf("params verbose		%d\n", params.verbose);
	printf("params scan_type	%s\n", params.scan_type == ALL ? "ALL" :
										params.scan_type == SYN ? "SYN" :
										params.scan_type == NUL ? "NULL" :
										params.scan_type == ACK ? "ACK" :
										params.scan_type == FIN ? "FIN" :
										params.scan_type == XMAS ? "XMAS" :
										params.scan_type == UDP ? "UDP" : "ERROR_TYPE_SCAN");
	printf("params ports_size	%u\n", params.ports_size);
	printf("params ports:		");
	for (uint32_t i = 0; i < params.ports_size ; i++)
	{
		if (i == 15)
			printf(" ... ");
		else if (i > 15 && i < params.ports_size - 4)
			;
		else
			printf("%hu ", params.ports[i]);
	}
	printf("\n");
	printf("params IPs:");
	for (int i = 0; params.ip[i] ; i++)
		printf("		%s\n", params.ip[i]);
	printf("\n");
}

void	debug_nmaps(t_nmap *nmap)
{
	printf("Debug nmap scan queue :\n");
	for (int i = 0; nmap[i].address; i++)
	{
		printf("IP		--> %s\n", nmap[i].address);
		printf("dport		--> %hu\n", nmap[i].dport);
		printf("sport		--> %hu\n", nmap[i].sport);
		printf("index		--> %zu\n", nmap[i].index);
		printf("scan_type	--> %s\n", nmap[i].scan_type == ALL ? "ALL" :
										nmap[i].scan_type == SYN ? "SYN" :
										nmap[i].scan_type == NUL ? "NULL" :
										nmap[i].scan_type == ACK ? "ACK" :
										nmap[i].scan_type == FIN ? "FIN" :
										nmap[i].scan_type == XMAS ? "XMAS" :
										nmap[i].scan_type == UDP ? "UDP" : "ERROR_TYPE_SCAN");
			printf("status		--> %s\n", nmap[i].status == AVAILABLE ? "AVAILABLE" :
										nmap[i].status == BUSY ? "BUSY":
										nmap[i].status == WAITING ? "WAITING":
										nmap[i].status == PREPARED ? "PREPARED":
										nmap[i].status == ERROR ? "ERROR": "ERROR THREAD STATUS");
		if (nmap[i].response)
			printf("scan response	--> %s : %s\n", nmap[i].response->service,
										nmap[i].response->state == OPEN ? "OPEN":
										nmap[i].response->state == CLOSED ? "CLOSED":
										nmap[i].response->state == FILTERED ? "FILTERED":
										nmap[i].response->state == OPENEDFILTERED ? "OPENEDFILTERED":
										nmap[i].response->state == CLOSEDFILTERED ? "CLOSEDFILTERED": "ERROR SCAN STATUS");
		printf("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n");
	}
	printf("\n\n");
}

void	debug_nmap(t_nmap nmap)
{
	printf("Debug current scan :\n");
	printf("IP		--> %s\n", nmap.address);
	printf("dport		--> %hu\n", nmap.dport);
	printf("sport		--> %hu\n", nmap.sport);
	printf("index		--> %zu\n", nmap.index);
	printf("scan_type	--> %s\n", nmap.scan_type == ALL ? "ALL" :
									nmap.scan_type == SYN ? "SYN" :
									nmap.scan_type == NUL ? "NULL" :
									nmap.scan_type == ACK ? "ACK" :
									nmap.scan_type == FIN ? "FIN" :
									nmap.scan_type == XMAS ? "XMAS" :
									nmap.scan_type == UDP ? "UDP" : "ERROR_TYPE_SCAN");
		printf("status		--> %s\n", nmap.status == AVAILABLE ? "AVAILABLE" :
									nmap.status == BUSY ? "BUSY":
									nmap.status == WAITING ? "WAITING":
									nmap.status == PREPARED ? "PREPARED":
									nmap.status == ERROR ? "ERROR": "ERROR THREAD STATUS");
	if (nmap.response)
		printf("scan response	--> %s : %s\n", nmap.response->service,
									nmap.response->state == OPEN ? "OPEN":
									nmap.response->state == CLOSED ? "CLOSED":
									nmap.response->state == FILTERED ? "FILTERED":
									nmap.response->state == OPENEDFILTERED ? "OPENEDFILTERED":
									nmap.response->state == CLOSEDFILTERED ? "CLOSEDFILTERED": "ERROR SCAN STATUS");
	printf("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n");
	printf("\n\n");
}
