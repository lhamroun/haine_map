#include "ft_nmap.h"

//*
static unsigned long	words(char const *s, char c)
{
	int			i;
	int			cpt;

	i = 0;
	cpt = 0;
	while (s[i])
	{
		while (s[i] == c && s[i] != '\0')
			i++;
		if (s[i] != c)
			cpt++;
		while (s[i] != c && s[i] != '\0')
			i++;
	}
	return (cpt);
}

static unsigned long	letter(char const *s, int start, char c)
{
	int			i;

	i = 0;
	while (s[start] != c && s[start])
	{
		start++;
		i++;
	}
	return (i);
}

char					**ft_strsplit(char const *s, char c)
{
	char			**wds;
	unsigned int	i;
	unsigned int	x;

	i = 0;
	x = 0;
	(void)words;
	(void)letter;
	if (!s || !c || !(wds = (char **)ft_memalloc(sizeof(char *) * words(s, c) + 1)))
		return (NULL);
	while (s[x])
	{
		while (s[x] == c && s[x])
			x++;
		if (s[x])
		{
			if (!(wds[i] = ft_strsub(s, x, letter(s, x, c))))
			{
				for (unsigned int e = 0; e < i; e++)
				{
					free(wds[e]);
					wds[e] = NULL;
				}
				free(wds);
				wds = NULL;
				return (NULL);
			}
			while (s[x] != c && s[x])
				x++;
			i++;
		}
	}
	wds[i] = NULL;
	return (wds);
}
//*/

