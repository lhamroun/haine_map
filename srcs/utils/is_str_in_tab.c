#include "ft_nmap.h"

int		is_str_in_tab(char const *str, char **tab)
{
	int		i = 0;

	while (tab[i])
	{
		if (!ft_strcmp(str, tab[i]))
			return 1;
		++i;
	}
	return 0;
}
