#include "ft_nmap.h"

char		*ft_strchr(char const *s, int c)
{
	return (ft_memchr(s, c, ft_strlen(s) + 1));
}
