#include "ft_nmap.h"

void	*ft_memalloc(size_t size)
{
	char	*buf;

	buf = NULL;
	if (!(buf = (void *)malloc(size)))
		return (NULL);
	return (ft_memset(buf, 0, size));
}
