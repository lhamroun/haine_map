#include "ft_nmap.h"

uint16_t	tcp_checksum(struct iphdr *ip, struct tcphdr *tcp)
{
    t_pseudo_hdr    pheader;
    uint16_t *ptr;
    int len, sum = 0;

    pheader.s_addr = ip->saddr;
    pheader.d_addr = ip->daddr;
    pheader.res = 0;
    pheader.protocol = ip->protocol;
    pheader.length = htons(sizeof(struct tcphdr));

    len = sizeof(t_pseudo_hdr);
    ptr = (uint16_t*)&pheader;
    while (len > 1) {
        sum += *ptr++;
        len -= 2;
    }

    len = sizeof(struct tcphdr);
    ptr = (uint16_t*)tcp;
    while (len > 1) {
        sum += *ptr++;
        len -= 2;
    }

    if (len > 0)
        sum += *(uint8_t*)ptr;

    while (sum >> 16)
        sum = (sum & 0xffff) + (sum >> 16);

    return (uint16_t)~sum;
}

uint16_t	checksum(void *b, int len)
{
	unsigned short	*buf;
	unsigned int	sum;
	unsigned short	result;

	buf = b;
	sum = 0;
	for (sum = 0; len > 1; len -= 2)
		sum += *buf++;
	if (len == 1)
		sum += *(unsigned char*)buf;
	sum = (sum >> 16) + (sum & 0xFFFF);
	sum += (sum >> 16);
	result = ~sum;
	return result;
}
