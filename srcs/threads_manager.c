#include "ft_nmap.h"

bool    join_threads(uint8_t speedup, int nb_job, pthread_t *threads)
{
        for (uint8_t i = 0; i < speedup && i < nb_job; i++)
                if (pthread_join(threads[i], NULL))
                        return false;
        return true;
}

bool    init_threads(int nb_job, t_params *params, pthread_t *threads, t_nmap *nmap)
{
        for (int i = 0; i < params->speedup && i < nb_job; i++)
        {
                if (i < nb_job && nmap[i].status == WAITING)
                        nmap[i].status = PREPARED;
                if (pthread_create(&threads[i], NULL, (void *)thread_exec, &nmap[i]))
                        return false;
        }
        return true;
}
