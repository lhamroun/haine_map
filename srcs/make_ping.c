#include "ft_nmap.h"

int isDuplicate(char **list, int size, const char *str)
{
    for (int i = 0; i < size; i++)
	{
        if (strcmp(list[i], str) == 0)
            return 1;
    }
    return 0;
}


char **get_unique_item(char **list, int size, int *newSize)
{
    int count = 0;  // Compteur pour le nombre de chaînes uniques
    char **uniqueList;

	uniqueList = (char **)ft_memalloc(size * sizeof(char *) + 1);
    if (!uniqueList)
		return NULL;
    for (int i = 0; i < size; i++)
	{
        if (!isDuplicate(uniqueList, count, list[i]))
		{
            uniqueList[count] = strdup(list[i]);
            count++;
        }
    }
    *newSize = count;  // Mise à jour de la taille de la nouvelle liste
	uniqueList[count] = NULL;
    return uniqueList;
}


t_params	*make_ping(t_params *params)
{
	struct s_args	args;
	args.quiet = false;
	char	**tab;
	char	**ban_list;
	int		newSize = 0;
	int		count = 0;
	char	**new_ip = NULL;

	args.quiet = 0;
	args.verbose = 0;
	args.help = 0;
	args.rtt = 1;
	args.ttl_max = 255;
	args.count = 1;

	tab = NULL;
	tab = get_unique_item(params->ip, listlen(params->ip), &newSize);
	ban_list = (char **)ft_memalloc((listlen(tab) + 1) * sizeof(char *));
	printf("- - - - - - - - - - - - - -\n");
	for (int i = 0; tab[i]; i++)
	{
		args.addr = ft_strdup(tab[i]);

		if (ft_ping(args))
			ban_list[count++] = ft_strdup(tab[i]);
		else
			printf("Host is UP\n");
		ft_memdel((void **)&args.addr);
	}
	printf("- - - - - - - - - - - - - -\n");
	if (listlen(ban_list) > 0)
	{
		for (int i = 0; ban_list[i]; i++)
			printf("host %s is down, no need to scan\n", ban_list[i]);
		printf("- - - - - - - - - - - - - -\n\n");
	}
	new_ip = (char **)ft_memalloc(sizeof(char *) * (listlen(tab) - listlen(ban_list) + 1));
	int k = 0;
	bool	banned;
	for (int i = 0; tab[i]; i++)
	{
		banned = false;
		for (int j = 0; ban_list[j]; j++)
		{
			if (!ft_strcmp(ban_list[j], tab[i]))
				banned = true;
		}
		if (!banned)
			new_ip[k++] = ft_strdup(tab[i]);
	}
	new_ip[k] = 0;

	for (int i = 0; tab[i]; i++)
	{
		ft_memdel((void **)&tab[i]);
	}
	ft_memdel((void **)&tab);
	for (int i = 0; ban_list[i]; i++)
	{
		ft_memdel((void **)&ban_list[i]);
	}
	ft_memdel((void **)&ban_list);
	for (int i = 0; params->ip[i]; i++)
	{
		ft_memdel((void **)&params->ip[i]);
	}
	ft_memdel((void **)&params->ip);
	params->ip = new_ip;

	return params;
}
