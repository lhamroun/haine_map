#include "ft_nmap.h"

void    usage(void)
{
	printf("Help Screen\nft_nmap [OPTIONS]\n--help Print this help screen\n \
		--ports ports to scan (eg: 1-10 or 1,2,3 or 1,5-15)\n \
		--ip IPV4 addresses to scan\n \
		--file File name containing IPV4 addresses to scan,\n \
		--speedup [250 max] number of parallel threads to use\n \
		--wireshark to enable packet traffic printing on stdout\n \
		--no-ping desactivate ping diagnostic before scanning (down host are ban from target scan list)\n \
		--scan SYN,NULL,FIN,XMAS,ACK,UDP\n");
}

char		**get_ip_from_file(char *filename)
{
	int		i;
	int		fd;
	int		ret;
	char	*tmp;
	char	**ip;

	i = 0;
	ret = 0;
	tmp = NULL;
	ip = (char **)ft_memalloc(sizeof(char *));
	if (!ip)
		return NULL;
	fd = open(filename, O_RDONLY);
	if (fd == -1)
	{
		ft_memdel((void **)&ip);
		return NULL;
	}
	while ((ret = get_next_line(fd, &tmp)) == 1)
	{
		ip[i] = ft_strdup(tmp);
		++i;
		tmp[ft_strlen(tmp) - 1] = 0;
		if (!is_ip_addr(tmp))
		{
			ft_memdel((void **)&ip);
			ft_memdel((void **)&tmp);
			return NULL;
		}
		ip = realloc(ip, sizeof(char *) * (i + 1));
		if (!ip) // leak
		{
			ft_memdel((void **)&tmp);
			return NULL;
		}
		ft_memdel((void **)&tmp);
	}
	ip[i] = NULL;
	if (ret == -1)
	{
		for (int j = 0; ip[j]; j++)
		{
			ft_memdel((void **)&ip[j]);
		}
		ft_memdel((void **)&ip);
		return NULL;
	}
	for (int j = 0; j < i; j++)
	{
		for (int k = j + 1; k < i; k++)
		{
			if (!ft_strcmp(ip[j], ip[k]))
			{
				printf("ip %s duplicate into %s\n", ip[j], filename);
				for (int e = 0; ip[e]; e++)
				{
					ft_memdel((void **)&ip[e]);
				}
				ft_memdel((void **)&ip);
				ft_memdel((void **)&tmp);
				return NULL;
			}
		}
	}
	ft_memdel((void **)&tmp);
	return ip;
}

t_params	*convert_options_to_params(t_options *opt)
{
	t_params	*params;

	params = ft_memalloc(sizeof(t_params));
	if (!params)
		return NULL;
	params->verbose = opt->verbose;
	params->ping = opt->ping;
	params->speedup = opt->speedup;
	params->scan_type = opt->scan_type;
	params->ports = opt->ports;
	params->ports_size = opt->ports_size;
	if (opt->file)
	{
		params->ip = get_ip_from_file(opt->file);
		if (!params->ip)
		{
			ft_memdel((void **)&params);
			return NULL;
		}
		for (int i = 0; params->ip[i]; i++)
		{
			params->ip[i][ft_strlen(params->ip[i]) - 1] = 0;
		}
		return params;
	}
	params->ip = (char **)ft_memalloc(sizeof(char *) * 2);
	if (!params->ip) // leak
	{
		ft_memdel((void **)&params);
		return NULL;
	}
	params->ip[0] = ft_strdup(opt->ip_addr);
	params->ip[1] = NULL;
	return params;
}

static char *get_result_string(enum e_scan_status status)
{
    char *ret = NULL;

    switch (status) {
        case OPEN:
           ret = "open";
           break;
        case CLOSED:
            ret = "closed";
            break;
        case FILTERED:
            ret = "filtered";
            break;
        case OPENEDFILTERED:
            ret = "open|filtered";
            break;
        case CLOSEDFILTERED:
            ret = "closed|filtered";
            break;
        case UNFILTERED:
            ret = "unfiltered";
            break;
        default:
            ret = "unknown";
            break;
    }
    return ret;
}

static char *get_scan_conclusion(t_response **scan_reponses, uint8_t responses_length)
{
    bool    filtered = false;
    bool    open_filtered = false;
    bool    closed_filtered = false;
    bool    open = false;
    bool    closed = false;
    bool    unfiltered = false;
    uint8_t index = 0;
    char    *res = NULL;

    for (index = 0; index < responses_length; index++) {
        switch (scan_reponses[index]->state) {
            case FILTERED: filtered = true; break;
            case UNFILTERED: unfiltered = true; break;
            case OPENEDFILTERED: open_filtered = true; break;
            case CLOSEDFILTERED: closed_filtered = true; break;
            case OPEN: open = true; break;
            case CLOSED: closed = true; break;
        }
    }
    if (open == true) res = "open";
    else if (closed == true) res = "closed";
    else if (unfiltered) res = "unfiltered";
	else if (filtered) res = "filtered";
	else if (open_filtered) res = "open|filtered";
    else if (closed_filtered) res = "closed|filtered";
    return res;
}

static void print_results(t_nmap *results)
{
    int len = 0;
    char *current_address = results[0].address;

    while (results[len].address != NULL) {

        printf("------------------------------\n");
        printf("ft_nmap scan report for %s\n", current_address);
		debug_network_address(get_ip_from_str(results[len].address));
		printf("DNS @		: %s\n\n", ft_strlen(results[len].dns) ? results[len].dns : "No DNS @");
        printf("%-6s %-13s %-22s %s\n", "PORT", "SERVICE", "STATE", "CONCLUSION");

        while (results[len].address != NULL && !ft_strcmp(current_address, results[len].address)) {

            if (results[len].status == ERROR) {
                printf("Error scanning port %u\n", results[len].dport);
                return;
            }

            printf("%-6u", results[len].dport);

            if (results[len].response->service) {
                printf(" %-13s", results[len].response->service);
            } else {
                printf(" %-13s", "unknown");
            }

            uint16_t current_port = results[len].dport;

            t_response *scan_responses[7 * sizeof(t_response*)]; //7 possible scans
            int8_t scans_length = 0;
            while (results[len + scans_length].address != NULL && !ft_strcmp(current_address, results[len + scans_length].address)
                && results[len + scans_length].dport == current_port) {
                scan_responses[scans_length] = results[len + scans_length].response;
                scans_length++;
            }
            uint8_t scan_index = 0;
            while (results[len].address != NULL && !ft_strcmp(current_address, results[len].address) && results[len].dport == current_port) {
                char result_string[22];
                char *status_string = get_result_string(results[len].response->state);
                switch (results[len].scan_type) {
                    case SYN:
                        snprintf(result_string, sizeof(result_string), "SYN(%s)", status_string);
                        break;
                    case ACK:
                        snprintf(result_string, sizeof(result_string), "ACK(%s)", status_string);
                        break;
                    case XMAS:
                        snprintf(result_string, sizeof(result_string), "XMAS(%s)", status_string);
                        break;
                    case NUL:
                        snprintf(result_string, sizeof(result_string), "NUL(%s)", status_string);
                        break;
                    case FIN:
                        snprintf(result_string, sizeof(result_string), "FIN(%s)", status_string);
                        break;
                    case UDP:
                        snprintf(result_string, sizeof(result_string), "UDP(%s)", status_string);
                        break;
                    default:
                        snprintf(result_string, sizeof(result_string), "SYN(%s)", status_string);
                        break;
                }
                printf(" %-22s", result_string);
                if (scan_index == 0)
                {
                    printf(" %s", get_scan_conclusion(scan_responses, scans_length));
                    scan_index++;
                }
                len++;
                if (results[len].address != NULL && !ft_strcmp(current_address, results[len].address)
                        && results[len].dport == current_port) {
                    printf("\n%-6s %-13s", "", "");
                }
            }
            printf("\n");
        }
        printf("\n");

        if (results[len].address == NULL) break;
        else
        {
            current_address = results[len].address;
        }
    }
}

int		main(int ac, char **av)
{
    t_options       *opt;
	t_params	    *params;
    t_nmap          *done;
    uint8_t         address_len;
    uint8_t         to_scan_len;
    struct tm       *tm_info;
    struct timeval  tv;
    struct timeval  tv_after;
	struct timeval  scan_duration;
	char            date_buffer[64];

    if (getuid())
	{
		printf("You need to be root to run ./ft_nmap\n");
		return EXIT_SUCCESS;
	}

	opt = parsing(ac - 1, av + 1);
    //debug_options(*opt);
	if (!opt || opt->helper)
	{
		ft_memdel((void **)&opt->ports);
		ft_memdel((void **)&opt->ip_addr);
		ft_memdel((void **)&opt);
		usage();
		return EXIT_FAILURE;
	}
	params = convert_options_to_params(opt);
	if (!params)
	{
		ft_memdel((void **)&opt->ip_addr);
		ft_memdel((void **)&opt->ports);
		//ft_memdel((void **)&opt->file);
		ft_memdel((void **)&opt);
		usage();
		return EXIT_FAILURE;
	}

    address_len = tab_2d_len(params->ip);
    gettimeofday(&tv, NULL);
    tm_info = localtime(&tv.tv_sec);
    strftime(date_buffer, sizeof(date_buffer), "%Y-%m-%d %H:%M:%S %Z", tm_info);
    printf("Starting ft_nmap at %s\n\n", date_buffer);

	if (params->ping)
		params = make_ping(params);
    to_scan_len = tab_2d_len(params->ip);
	done = process(params);
    gettimeofday(&tv_after, NULL);
    ft_memdel((void **)&opt->ip_addr);
	ft_memdel((void **)&opt);
	ft_memdel((void **)&params->ports);
	for (int i = 0; params->ip[i]; i++)
	{
		ft_memdel((void **)&params->ip[i]);
	}
	ft_memdel((void **)&params->ip);
	ft_memdel((void **)&params);

	if (!done)
	{
		usage();
		return EXIT_FAILURE;
	}
	print_results(done);
	timeval_subtract(&scan_duration, &tv_after, &tv);
    printf("Ft_nmap done: %d IP addresses (%d host(s) up) scanned in %lu.%ld seconds\n",
			address_len, to_scan_len, scan_duration.tv_sec, scan_duration.tv_usec / 1000);
	for (int i = 0; done[i].response; i++)
	{
		ft_memdel((void **)&done[i].response->service);
		ft_memdel((void **)&done[i].response);
	}
	for (int i = 0; done[i].dns; i++)
	{
		ft_memdel((void **)&done[i].dns);
	}
	for (int i = 0; done[i].address; i++)
	{
		ft_memdel((void **)&done[i].address);
	}
	ft_memdel((void **)&done);
	return EXIT_SUCCESS;
}
