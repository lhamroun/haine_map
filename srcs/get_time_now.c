#include "ft_nmap.h"

uint64_t    get_time_now(void)
{
    struct timeval    timestamp;

    gettimeofday(&timestamp, NULL);
    return ((uint64_t)timestamp.tv_sec * 1000000 + (uint64_t)timestamp.tv_usec);
}