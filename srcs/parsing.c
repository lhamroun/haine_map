#include "ft_nmap.h"

bool	is_ip_addr(char *addr)
{
	struct addrinfo		hints;
	struct addrinfo		*server;

	if (addr[ft_strlen(addr) - 1] == '.')
		return NULL;
	ft_memset(&hints, 0, sizeof(struct addrinfo));
	server = NULL;
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_RAW;
	hints.ai_flags = AI_CANONNAME;
	if (getaddrinfo(addr, NULL, &hints, &server) != 0)
		return false;
	freeaddrinfo(server);
	return true;
}

t_options	set_default_options(void)
{
	t_options	opt;

	ft_memset(&opt, 0, sizeof(t_options));
	opt.helper		= DEFAULT_HELPER;
	opt.verbose		= DEFAULT_VERBOSE;
	opt.ping		= true;
	opt.speedup		= DEFAULT_THREAD;
	opt.scan_type	= DEFAULT_SCAN;
	opt.file		= DEFAULT_FILE;
	opt.ip_addr		= DEFAULT_IP_ADDR;
	opt.ports		= check_ports(DEFAULT_PORTS);
	if (opt.ports)
		opt.ports_size	= get_ports_size(opt.ports);
	return opt;
}

bool    check_opt_for_parse(char *str)
{
	if (ft_strcmp(str, "--speedup")
			&& ft_strcmp(str, "--ports")
			&& ft_strcmp(str, "--ip")
			&& ft_strcmp(str, "--scan")
			&& ft_strcmp(str, "--help")
			&& ft_strcmp(str, "--no-ping")
			&& ft_strcmp(str, "--wireshark")
			&& ft_strcmp(str, "--file"))
		return true;
	return false;
}

bool	check_dash_entry(char **tmp)
{
	if (!tmp)
		return false;
	if (listlen(tmp) != 2)
		return false;
	if (!str_is_number(tmp[0]) || !str_is_number(tmp[1]))
		return false;
	if (ft_atoi(tmp[0]) > ft_atoi(tmp[1]))
	{
		printf("Your port range %d-%d is backwards. Did you mean %d-%d?\n",
			ft_atoi(tmp[0]),
			ft_atoi(tmp[1]),
			ft_atoi(tmp[1]),
			ft_atoi(tmp[0]));
		return false;
	}
	return true;
}

bool	check_invalid_ports(uint16_t *ports, uint32_t ports_size)
{
	uint32_t	tmp;

	for (uint32_t i = 0; i < ports_size; i++)
	{
		tmp = (uint32_t)ports[i];
		if (tmp < 1 || tmp > USHRT_MAX)
		{
			printf("your port %u is out of the bounded (0<port<=65535)\n", tmp);
			return false;
		}
		for (uint32_t j = i + 1; j < ports_size; j++)
		{
			if (tmp == ports[j])
			{
				printf("port %u duplicate\n", tmp);
				return false;
			}
		}
	}
	return true;
}

uint32_t	get_ports_size(uint16_t *ports)
{
	uint32_t	i;

	i = 0;
	while (ports[i] != 0)
		i++;
	return i;
}

uint16_t	*check_ports(char *str)
{
	char		**tab;
	char		**tmp;
	uint16_t	*ports;
	uint32_t	ports_size;

	tab = NULL;
	tmp = NULL;
	ports = NULL;
	ports_size = 0;
	tab = ft_strsplit(str, ',');
	ports = ft_memalloc(1);
	if (!ports)
		return NULL;
	if (!tab)
	{
		ft_memdel((void **)&ports);
		return NULL;
	}
	for (int i = 0; tab[i]; i++)
	{
		if (str_is_number(tab[i]))
		{
			if (ft_atoi(tab[i]) < 0 || ft_atoi(tab[i]) > 65535)
			{
				ft_memdel((void **)&ports);
				for (int k = 0; tab[k]; k++)
				{
					ft_memdel((void **)&tab[k]);
				}
				ft_memdel((void **)&tab);
				return NULL;
			}
			ports_size++;
			ports = realloc(ports, sizeof(uint16_t) * (ports_size + 1));
			if (!ports)
			{
				for (int k = 0; tab[k]; k++)
				{
					free(tab[k]);
					tab[k] = NULL;
				}
				free(tab);
				tab = NULL;
				return NULL;
			}
			ports[ports_size - 1] = (uint16_t)ft_atoi(tab[i]);
		}
		else if (ft_strstr(tab[i], "-"))
		{
			tmp = ft_strsplit(tab[i], '-');
			if (!tmp)
			{
				free(ports);
				ports = NULL;
				for (int k = 0; tab[k]; k++)
				{
					free(tab[k]);
					tab[k] = NULL;
				}
				free(tab);
				tab = NULL;
				return NULL;
			}
			if (!check_dash_entry(tmp))
			{
				free(ports);
				ports = NULL;
				for (int k = 0; tab[k]; k++)
				{
					free(tab[k]);
					tab[k] = NULL;
				}
				free(tab);
				tab = NULL;
				for (int k = 0; tmp[k]; k++)
				{
					free(tmp[k]);
					tmp[k] = NULL;
				}
				free(tmp);
				tmp = NULL;
				return NULL;
			}
			ports = realloc(ports, sizeof(uint16_t) * (ft_atoi(tmp[1]) - ft_atoi(tmp[0]) + 1));
			if (!ports)
			{
				for (int k = 0; tab[k]; k++)
				{
					free(tab[k]);
					tab[k] = NULL;
				}
				free(tab);
				tab = NULL;
				for (int k = 0; tmp[k]; k++)
				{
					free(tmp[k]);
					tmp[k] = NULL;
				}
				free(tmp);
				tmp = NULL;
				return NULL;
			}
			for (int j = 0; j < ft_atoi(tmp[1]) - ft_atoi(tmp[0]) + 1; j++)
				ports[ports_size + j] = (uint16_t)ft_atoi(tmp[0]) + j;
			ports_size = ports_size + ft_atoi(tmp[1]) - ft_atoi(tmp[0]) + 1;
			free(tmp[0]);
			tmp[0] = NULL;
			free(tmp[1]);
			tmp[1] = NULL;
			free(tmp);
			tmp = NULL;
		}
		else
		{
			ft_memdel((void **)&ports);
			for (int k = 0; tab[k]; k++)
			{
				ft_memdel((void **)&tab[k]);
			}
			ft_memdel((void **)&tab);
			return NULL;
		}
	}
	for (int k = 0; tab[k]; k++)
	{
		ft_memdel((void **)&tab[k]);
	}
	ft_memdel((void **)&tab);
	if (!check_invalid_ports(ports, ports_size))
	{
		free(ports);
		ports = NULL;
		return NULL;
	}
	ports[ports_size] = 0;
	return ports;
}

char		**alloc_scan_type_tab(void)
{
	char **tab;

	tab = ft_memalloc(sizeof(char *) * 7);
	if  (!tab)
		return NULL;
	tab[0] = ft_strdup("SYN");
	if (!tab[0])
	{
		free(tab);
		return NULL;
	}
	tab[1] = ft_strdup("NULL");
	if (!tab[1])
	{
		free(tab[0]);
		free(tab);
		return NULL;
	}
	tab[2] = ft_strdup("ACK");
	if (!tab[2])
	{
		free(tab[0]);
		free(tab[1]);
		free(tab);
		return NULL;
	}
	tab[3] = ft_strdup("FIN");
	if (!tab[3])
	{
		free(tab[0]);
		free(tab[1]);
		free(tab[2]);
		free(tab);
		return NULL;
	}
	tab[4] = ft_strdup("XMAS");
	if (!tab[4])
	{
		free(tab[0]);
		free(tab[1]);
		free(tab[2]);
		free(tab[3]);
		free(tab);
		return NULL;
	}
	tab[5] = ft_strdup("UDP");
	if (!tab[0])
	{
		free(tab[0]);
		free(tab[1]);
		free(tab[2]);
		free(tab[3]);
		free(tab[4]);
		free(tab);
		return NULL;
	}
	return tab;
}

bool	check_scan_arg(char *input_scan, char **ref_scans)
{
	char	**split_input_scan;
	int		size;

	split_input_scan = ft_strsplit(input_scan, ',');
	if (!split_input_scan)
		return false;
	size = listlen(split_input_scan);
	for (int i = 0; i < size; i++)
	{
		if (!is_str_in_tab(split_input_scan[i], ref_scans))
		{
			for (int j = 0; j < size; j++)
			{
				ft_memdel((void **)&split_input_scan[j]);
			}
			ft_memdel((void **)&split_input_scan);
			return false;
		}
	}
	for (int j = 0; j < size; j++)
	{
		ft_memdel((void **)&split_input_scan[j]);
	}
	ft_memdel((void **)&split_input_scan);
	return true;
}

enum e_scan	convert_scan_list_to_scan_type(char *av)
{
	enum e_scan	scan_type;
	char		**input_scan;

	scan_type = DEFAULT_SCAN;
	input_scan = ft_strsplit(av, ',');
	if (!input_scan)
	{
		return SCAN_ERROR;
	}
	for (int i = 0; i < listlen(input_scan); i++)
	{
		if (!ft_strcmp(input_scan[i], "SYN"))
			scan_type |= SYN;
		else if (!ft_strcmp(input_scan[i], "NULL"))
			scan_type |= NUL;
		else if (!ft_strcmp(input_scan[i], "FIN"))
			scan_type |= FIN;
		else if (!ft_strcmp(input_scan[i], "ACK"))
			scan_type |= ACK;
		else if (!ft_strcmp(input_scan[i], "XMAS"))
			scan_type |= XMAS;
		else if (!ft_strcmp(input_scan[i], "UDP"))
			scan_type |= UDP;
	}
	if (scan_type == (SYN | NUL | ACK | XMAS | UDP | FIN))
		scan_type = ALL;
	for (int i = 0; input_scan[i]; i++)
	{
		free(input_scan[i]);
		input_scan[i] = NULL;
	}
	free(input_scan);
	input_scan = NULL;
	return scan_type;
}

t_options	*parsing(int ac, char **av)
{
	t_options		*opt;
	uint16_t		*ports;
	t_options_check	opt_check;
	char			**scan_type;

	ports = NULL;
	scan_type = NULL;
	opt = ft_memalloc(sizeof(t_options));
	if (!opt)
	{
		printf("ERROR: malloc t_options object fail\n");
		return (NULL);
	}
	ft_memset(&opt_check, 0, sizeof(t_options_check));
	*opt = set_default_options();
	if (!(scan_type = alloc_scan_type_tab()))
	{
		free(opt);
		printf("ERROR: malloc scan_type fail\n");
		return (NULL);
	}
	for (int i = 0; i < ac; i++)
	{
		if (!ft_strcmp(av[i], "--help"))
		{
			++opt_check.n_helper;
			opt->helper = true;
		}
		else if (!ft_strcmp(av[i], "--wireshark"))
		{
			++opt_check.n_verbose;
			opt->verbose = true;
		}
		else if (!ft_strcmp(av[i], "--no-ping"))
		{
			++opt_check.n_ping;
			opt->ping = false;
		}
		else if (!ft_strcmp(av[i], "--ip"))
		{
			if (i >= listlen(av) - 1 || !is_ip_addr(av[i + 1]) || opt_check.n_ip_addr > 0)
			{
				opt_check.n_helper = 42;
				break ;
			}
			++opt_check.n_ip_addr;
			opt->ip_addr = ft_strdup(av[i + 1]);
			if (!opt->ip_addr)
			{
				opt_check.n_helper = 42;
				break ;
			}
			++i;
		}
		else if (!ft_strcmp(av[i], "--file"))
		{
			if (i >= listlen(av) - 1)
			{
				opt_check.n_helper = 42;
				break ;
			}
			++opt_check.n_file;
			opt->file = av[i + 1];
			++i;
		}
		else if (!ft_strcmp(av[i], "--speedup")) // nb of threads
		{
			if (i >= listlen(av) - 1 || !str_is_number(av[i + 1])
					|| ft_atoi(av[i + 1]) < 1
					|| ft_atoi(av[i + 1]) > MAX_THREADS)
			{
				opt_check.n_helper = 42;
				break ;
			}
			++opt_check.n_speedup;
			opt->speedup = ft_atoi(av[i + 1]);
			++i;
		}
		else if (!ft_strcmp(av[i], "--ports")) // ports to scan
		{
			ports = check_ports(av[i + 1]);
			if (i >= listlen(av) - 1 || !ports)
			{
				opt_check.n_helper = 42;
				break ;
			}
			++opt_check.n_ports;
			if (opt->ports)
			{
				free(opt->ports);
				opt->ports = NULL;
			}
			opt->ports = ports;
			opt->ports_size = get_ports_size(ports);
			++i;
		}
		else if (!ft_strcmp(av[i], "--scan")) // scan type
		{
			if (i >= listlen(av) - 1 || !check_scan_arg(av[i + 1], scan_type))
			{
				opt->scan_type = SCAN_ERROR;
				opt_check.n_helper = 42;
				break ;
			}
			++opt_check.n_scan_type;
			opt->scan_type = convert_scan_list_to_scan_type(av[i + 1]);
			++i;
		}
		else // unknown option or hostname
		{
			printf("ft_nmap: unknown option %s\n", av[i]);
			opt_check.n_helper = 42;
			break ;
		}
	}
	if (opt_check.n_helper > 1
			|| opt_check.n_verbose > 1
			|| opt_check.n_ping > 1
			|| opt_check.n_speedup > 1
			|| opt_check.n_ports > 1
			|| opt_check.n_scan_type > 1
			|| opt_check.n_ip_addr > 1
			|| opt_check.n_file > 1
			|| (opt_check.n_file == 1 && opt_check.n_ip_addr != 0)
			|| (opt_check.n_ip_addr == 1 && opt_check.n_file != 0)
			|| (!opt_check.n_ip_addr && !opt_check.n_file))
	{
		opt->helper = true;
	}
	for (int i = 0; i < 6; i++)
		free(scan_type[i]);
	free(scan_type);
	return opt;
}
