#include "ft_nmap.h"

struct sockaddr_in      get_sock_addr(t_nmap *job)
{
        struct sockaddr_in      target;

        ft_memset(&target, 0, sizeof(target));
        target.sin_family = AF_INET;
        target.sin_port = htons(job->dport);
        //inet_pton(AF_INET, job->address, &(target.sin_addr));
		target.sin_addr.s_addr = get_ip_from_str(job->address);
        return target;
}

uint32_t	get_my_ip_addr(void)
{
	struct ifaddrs	*ifap;
	struct ifaddrs	*tmp;
	uint32_t		ret;

	ret = 0;
	if (getifaddrs(&ifap) == -1)
		return 0;
	tmp = ifap;
	while (ifap->ifa_next)
	{
		if (ft_strncmp(ifap->ifa_name, "lo", 2) && ((struct sockaddr_in *)ifap->ifa_addr)->sin_family == AF_INET)
		{
			ret = ((struct sockaddr_in *)ifap->ifa_addr)->sin_addr.s_addr;
			ifap = tmp;
			freeifaddrs(ifap);
			return ret;
		}
		ifap = ifap->ifa_next;
	}
	ifap = tmp;
	freeifaddrs(ifap);
	return 0;
}

char	*get_service_name(enum e_scan scan_type, uint16_t port)
{
	char				protocol[4];
	struct servent		*tmp;
	void				*ret;

	ft_memset(protocol, 0, sizeof(protocol));
	// set protocol name to get service name
	if (scan_type == UDP)
		ret = ft_memcpy(protocol, "udp", sizeof("udp"));
	else
		ret = ft_memcpy(protocol, "tcp", sizeof("tcp"));
	if (!ret)
		return NULL;
	tmp = getservbyport(htons(port), protocol);
	if (tmp)
		return ft_strdup(tmp->s_name);
	return NULL;
}

uint32_t	get_ip_from_str(char *addr)
{
	uint32_t		ipv4;
	struct addrinfo	*tmp;

	tmp = setServerInfo(addr);
	ipv4 = ((struct sockaddr_in *)tmp->ai_addr)->sin_addr.s_addr;
	freeaddrinfo(tmp);
	return ipv4;
}
