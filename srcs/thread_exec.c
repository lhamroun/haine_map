#include "ft_nmap.h"

static ssize_t check_ip(struct iphdr *ip, uint32_t dest_address)
{
	if (ip->saddr != dest_address || ip->daddr != get_my_ip_addr())
		return -1;
	return ip->protocol;
}

static ssize_t	check_recv_tcp(struct tcphdr *tcp, uint16_t sport, uint16_t dport)
{
	if (ntohs(tcp->th_sport) != sport)
	{
		return -1;
	}
	if (ntohs(tcp->th_dport) != dport)
	{
		return -1;
	}
	return 0;
}

static ssize_t	check_recv_icmp(char *recv_buffer, struct sockaddr_in target, t_nmap *job, int nb_try)
{
    struct iphdr    *ip;
    struct iphdr   *encapsulated_ip;
    struct udphdr   *encapsulated_udp;
    struct icmphdr  *packet;

    packet = (struct icmphdr *)((char *)recv_buffer + ((struct iphdr *)recv_buffer)->ihl * 4);
    ip = (struct iphdr*)recv_buffer;
    if (ip->saddr != target.sin_addr.s_addr || ip->daddr != get_my_ip_addr())
    {
        return 1;
    }
    encapsulated_ip = (struct iphdr*)((char*)packet + sizeof (struct icmphdr));
    if (encapsulated_ip->daddr != target.sin_addr.s_addr || encapsulated_ip->saddr != get_my_ip_addr())
    {
        return 1;
    }
    encapsulated_udp = (struct udphdr*)((char*)packet + sizeof (struct icmphdr) + sizeof (struct iphdr));
    if (ntohs(encapsulated_udp->uh_sport) != (job->sport + nb_try)
        || ntohs(encapsulated_udp->uh_dport) != job->dport)
    {
        return 1;
    }
	return 0;
}


static ssize_t	check_recv_udp(struct udphdr *udp, uint16_t sport, uint16_t dport)
{
	if (ntohs(udp->source) != sport)
	{
		return -1;
	}
	if (ntohs(udp->dest) != dport)
	{
		return -1;
	}
	return 0;
}

static int		get_socket_fd(int sock_type)
{
	int		opt = 1;
	int		sockfd;

	if ((sockfd = socket(AF_INET, SOCK_RAW | SOCK_NONBLOCK, sock_type)) < 0)
		return -1;
	if (setsockopt(sockfd, IPPROTO_IP, IP_HDRINCL, &opt, sizeof(int)) < 0)
		return -1;
	return sockfd;
}

void	set_additional_status(t_nmap *job, int nb_try)
{
	if (nb_try > SCAN_MAX_RETRIES)
	{
		if (job->scan_type == SYN || job->scan_type == ACK)
			job->response->state = FILTERED;
		if (job->scan_type == NUL ||
				job->scan_type == FIN ||
				job->scan_type == XMAS ||
				job->scan_type == UDP)
			job->response->state = OPENEDFILTERED;
	}
}

static void	prepare_tcp_header(struct tcphdr *tcp, t_nmap *job, int nb_try, struct iphdr *ip)
{
	//uint32_t        tmp;

	//if (inet_pton(AF_INET, job->address, &tmp) != 1) return;

	ft_memset(tcp, 0, sizeof(struct tcphdr));
	switch (job->scan_type)
	{
		case SYN:
			tcp->th_flags |= TH_SYN;
			break;
		case NUL:
			break;
		case ACK:
			tcp->th_flags |= TH_ACK;
			break;
		case FIN:
			tcp->th_flags |= TH_FIN;
			break;
		case XMAS:
			tcp->th_flags |= TH_FIN;
			tcp->th_flags |= TH_PUSH;
			tcp->th_flags |= TH_URG;
			break;
		default:
			break;
	}
	tcp->th_sport = htons(job->sport + (uint16_t)nb_try);
	tcp->th_dport = htons(job->dport);
	tcp->th_seq = 0;
	tcp->th_ack = 0;
	tcp->th_off = (uint8_t)sizeof(struct tcphdr) / 4;
	tcp->th_win = htons(1024);
	tcp->th_sum = 0;
	tcp->th_sum = tcp_checksum(ip, tcp);
}

static void	prepare_udp_header(struct udphdr *udp, t_nmap *job, int nb_try, struct iphdr *ip)
{
    uint32_t        tmp;

    if (inet_pton(AF_INET, job->address, &tmp) != 1) return;
    (void)ip;
    ft_memset(udp, 0, sizeof(struct udphdr));
    udp->uh_sport = htons(job->sport + (uint16_t)nb_try);
    udp->uh_dport = htons(job->dport);
    udp->uh_sum = 0;
    udp->uh_ulen = htons(sizeof (struct udphdr) + 12);
    udp->uh_sum = 0;
}

static void	prepare_ip_header(struct iphdr *ip, uint32_t s_addr, uint32_t d_addr, int socktype)
{
	ip->ihl = 5;
	ip->version = 4;
	ip->tos = 0;
	ip->id = 30245;
	ip->frag_off = 0;
	ip->ttl = 64;
    if (socktype == IPPROTO_TCP) {
        ip->tot_len = sizeof(struct iphdr) + sizeof(struct tcphdr);
        ip->protocol = IPPROTO_TCP;
    }
    else if (socktype == IPPROTO_UDP) {
        ip->tot_len = sizeof(struct iphdr) + sizeof(struct udphdr) + 12;
        ip->protocol = IPPROTO_UDP;
    }
	ip->saddr = s_addr;
	ip->daddr = d_addr;
	ip->check = 0;
	ip->check = checksum(ip, sizeof(struct iphdr));
}

void	send_packet(int sockfd, struct sockaddr_in target, t_nmap *job, ssize_t *ret, int nb_try)
{
	struct iphdr	ip;
	struct tcphdr	tcp;
	struct udphdr	udp;
	uint32_t		dest_ip;
	char			packet[sizeof(struct iphdr) + sizeof(struct tcphdr) + 1];

	switch (job->scan_type)
	{
		case SYN:
		case NUL:
		case ACK:
		case FIN:
		case XMAS:
            memset(packet, 0, sizeof(struct iphdr) + sizeof(struct tcphdr) + 1);
            //inet_pton(AF_INET, job->address, &dest_ip);
            dest_ip = get_ip_from_str(job->address);
			prepare_ip_header(&ip, get_my_ip_addr(), dest_ip, IPPROTO_TCP);
			prepare_tcp_header(&tcp, job, nb_try, &ip);
			ft_memcpy(packet, &ip, sizeof(struct iphdr));
			ft_memcpy(packet + sizeof(struct iphdr), &tcp, sizeof(struct tcphdr));
			if (job->verbose)
			{
				printf("- - - - - Sending - - - - -\n");
				debug_ip((struct iphdr *)packet);
				debug_tcp((struct tcphdr *)((char *)packet + sizeof(struct iphdr)));
			}
			*ret = sendto(sockfd, packet, ((struct iphdr *)packet)->tot_len, 0, (struct sockaddr *)&target, sizeof(target));
			if (*ret < 0)
				printf("sendto return -1\n");
			break;
		case UDP:
            memset(packet, 0, sizeof(struct iphdr) + sizeof(struct udphdr) + 12 + 1);
            inet_pton(AF_INET, job->address, &dest_ip);
            prepare_ip_header(&ip, get_my_ip_addr(), dest_ip, IPPROTO_UDP);
            prepare_udp_header(&udp, job, nb_try, &ip);
            char *tmp = (char*)packet;
            tmp[sizeof (struct iphdr) + sizeof (struct udphdr) + 2] = 0x10;
            ft_memcpy(packet, &ip, sizeof(struct iphdr));
            ft_memcpy(packet + sizeof(struct iphdr), &udp, sizeof(struct udphdr));
            if (job->verbose)
            {
                printf("- - - - - Sending - - - - -\n");
                debug_udp((struct udphdr *)&udp);
            }
            *ret = sendto(sockfd, packet, ((struct iphdr *)packet)->tot_len, 0, (struct sockaddr *) &target, sizeof(target));
            if (*ret <= 0)
                printf("sendto return -1\n");
            break;
		default:
			job->status = ERROR;
			close(sockfd);
			pthread_exit(NULL);
	}
}

bool	analyze_recv_packet(ssize_t bytes_received, char *recv_buffer, t_nmap *job)
{
	void	*tmp;

	tmp = NULL;
	if (bytes_received < 0)
	{
		if (errno == EAGAIN || errno == EWOULDBLOCK)
		{
			if (job->scan_type == UDP)
				job->response->state = OPENEDFILTERED;
			else
				job->response->state = FILTERED;
			return true;
		}
		else
		{
			perror("recvfrom");
			job->status = ERROR;
			return true;
		}
	}
	else if (bytes_received == 0)
	{
		return true;
	}
	else
	{
		tmp = (void *)((char *)recv_buffer + ((struct iphdr *)recv_buffer)->ihl * 4);
		if (job->scan_type == UDP)
		{
			if (((struct icmphdr *)tmp)->type == ICMP_DEST_UNREACH && ((struct icmphdr *)tmp)->code == ICMP_PORT_UNREACH)
				job->response->state = CLOSED;
			else if (((struct icmphdr *)tmp)->type == ICMP_DEST_UNREACH)
				job->response->state = FILTERED;
			else
				job->response->state = OPEN;
		}
		else
		{
			switch (job->scan_type)
			{
				case SYN:
					if ((((struct tcphdr *)tmp)->th_flags & TH_SYN) && (((struct tcphdr *)tmp)->th_flags & TH_ACK))
					{
						job->response->state = OPEN;
					}
					else if ((((struct tcphdr *)tmp)->th_flags & TH_RST))
						job->response->state = CLOSED;
					break;
				case NUL:
				case FIN:
				case XMAS:
					if ((((struct tcphdr *)tmp)->th_flags & TH_RST))
						job->response->state = CLOSED;
					else if (((struct iphdr *)recv_buffer)->protocol == 1 && ((struct icmphdr *)tmp)->type == 3) // ICMP protocol = 1
						job->response->state = OPENEDFILTERED;
					else
						job->response->state = OPEN;
					break;
				case ACK:
					if ((((struct tcphdr *)tmp)->th_flags & TH_RST))
						job->response->state = UNFILTERED;
					else
						job->response->state = FILTERED;
					break;
				default:
					break;
			}
		}
	}
	return false;
}

void scan_port(t_nmap *job)
{
	int					send_sockfd, r_udp_socket, r_tcp_socket, r_icmp_socket = -1;
	char				recv_buffer[4096];
	ssize_t				ret;
	int					nb_try = 0;
	struct sockaddr_in	source;
	struct sockaddr_in	target;
	socklen_t			source_len = sizeof(source);
	struct pollfd       poll_set[3];
	uint64_t            start;
	ssize_t				recv_len = 0;;

	r_icmp_socket = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	r_udp_socket = socket(AF_INET, SOCK_RAW, IPPROTO_UDP);
	r_tcp_socket = socket(AF_INET, SOCK_RAW, IPPROTO_TCP);
	poll_set[0].fd = r_icmp_socket;
	poll_set[0].events = POLLIN | POLLERR;
	poll_set[1].fd = r_udp_socket;
	poll_set[1].events = POLLIN | POLLERR;
	poll_set[2].fd = r_tcp_socket;
	poll_set[2].events = POLLIN | POLLERR;
	while (nb_try <= SCAN_MAX_RETRIES)
	{
		target = get_sock_addr(job);
		send_sockfd = get_socket_fd(job->scan_type == UDP ? IPPROTO_UDP : IPPROTO_TCP);

		if (send_sockfd < 0 || r_udp_socket < 0 || r_icmp_socket < 0 || r_tcp_socket < 0)
		{
			job->status = ERROR;
			nb_try++;
			continue ;
		}

		send_packet(send_sockfd, target, job, &ret, nb_try);
        close(send_sockfd);

        start = get_time_now();
		while (get_time_now() - start < 1000 * 100)
		{
			int poll_result = poll(poll_set, 3, 1);
			if (poll_result == -1)
            {
                job->status = ERROR;
                goto err;
            }
			else
			{
				for (int i = 0; i < 3; i++)
				{
					if (poll_set[i].revents & POLLIN)
					{
						int protocol;
						ft_memset(&recv_buffer, 0, sizeof(recv_buffer));
						ret = recvfrom(poll_set[i].fd , recv_buffer , sizeof(recv_buffer), 0, &source, (socklen_t *)&source_len);
						recv_len = ret;
						if (ret < 0)
						{
                            job->status = ERROR;
                            goto err;
						}
						else if (ret == 0)
						{
							printf("Error recvfrom 0\n");
							ret = -1;
						}
						else
						{
							protocol = check_ip((void*)recv_buffer, target.sin_addr.s_addr);
							if (job->verbose)
							{
								printf("- - - - - Receiving - - - - -\n");
								debug_ip((void *)recv_buffer);
								if (protocol == IPPROTO_ICMP)
									debug_icmp((void *)((char *)recv_buffer + sizeof(struct iphdr)));
								if (protocol == IPPROTO_UDP)
									debug_udp((void *)((char *)recv_buffer + sizeof(struct iphdr)));
								if (protocol == IPPROTO_TCP)
									debug_tcp((void *)((char *)recv_buffer + sizeof(struct iphdr)));
							}
							if (protocol == IPPROTO_ICMP)
							{
								ret = check_recv_icmp((char *)recv_buffer, target, job, nb_try);
								if (!ret)
									goto analyze_packet;
							}
							else if (protocol == IPPROTO_UDP)
							{
								ret = check_recv_udp((struct udphdr *)((char *)recv_buffer + sizeof(struct iphdr)), job->dport, job->sport);
								if (!ret)
									goto analyze_packet;
							}
							else if (protocol == IPPROTO_TCP)
							{
								ret = check_recv_tcp((struct tcphdr *)((char *)recv_buffer + sizeof(struct iphdr)), job->dport, job->sport);
								if (!ret)
									goto analyze_packet;
							}
							else
                            {
                               ret = -1;
                            }
						}
					}
				}
			}
		}
		if (!ret)
		{
			analyze_packet:
			ret = analyze_recv_packet(recv_len, recv_buffer, job);
			if (!ret)
			{
				goto end;
			}
			nb_try++;
		}
		else
			nb_try++;
	}

	end:
	set_additional_status(job, nb_try);
	job->response->service = get_service_name(job->scan_type, job->dport);
    err:
    close(r_icmp_socket);
	close(r_udp_socket);
	close(r_tcp_socket);
}

void	thread_exec(t_nmap *job)
{
	while (1)
	{
		if (job->address == NULL)
			return;
		if (job->status == PREPARED)
		{
			job->status = BUSY;
			scan_port(job);
            if (job->status == ERROR) return;
			else job->status = AVAILABLE;
		}
	}
}
