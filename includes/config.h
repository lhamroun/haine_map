#ifndef CONFIG_H
#define CONFIG_H

#include <stdbool.h>
#include <stdlib.h>

typedef enum		e_scan
{
	ALL			= 0,
	SYN			= 1,
	NUL			= 2,
	ACK			= 4,
	FIN			= 8,
	XMAS		= 16,
	UDP			= 32,
	SCAN_ERROR	= 64
}					t_scan;

#define SCAN_MAX_RETRIES    1

#define DEFAULT_HELPER		false
#define DEFAULT_VERBOSE		false
#define DEFAULT_THREAD		0
#define DEFAULT_SCAN		ALL
#define DEFAULT_FILE		NULL
#define DEFAULT_IP_ADDR		NULL
#define DEFAULT_PORTS		"1-1024"

#endif
