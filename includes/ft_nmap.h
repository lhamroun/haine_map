#ifndef FT_NMAP_H
#define FT_NMAP_H

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <netdb.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/time.h>

#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <netinet/ip_icmp.h>
#include <netinet/in.h>
#include <netinet/tcp.h>

#include <poll.h>
#include <ifaddrs.h>

#include "config.h"
#include "spec.h"
#include "utils.h"
#include "inet.h"
#include "ft_ping.h"

typedef enum		e_thread_status
{
	AVAILABLE	= 0,
	BUSY		= 1,
	WAITING		= 2,
	PREPARED	= 3,
	ERROR		= 4,
}					t_thread_status;


typedef enum		e_scan_status
{
	FILTERED		= 0,
	OPEN			= 1,
	CLOSED			= 2,
	OPENEDFILTERED	= 3,
	CLOSEDFILTERED	= 4,
	UNFILTERED		= 5
}					t_scan_status;

typedef struct		s_options
{
	bool			helper;
	bool			verbose;
	bool			ping;
	uint8_t			speedup;
	enum e_scan		scan_type;
	char			*file;
	char			*ip_addr;
	uint16_t		*ports;
	uint32_t		ports_size;
}					t_options;

typedef struct		s_options_check
{
	uint8_t			n_helper;
	uint8_t			n_verbose;
	uint8_t			n_ping;
	uint8_t			n_scan_type;
	uint8_t			n_speedup;
	uint8_t			n_file;
	uint8_t			n_ip_addr;
	uint8_t			n_ports;
}					t_options_check;

typedef struct		s_params
{
	bool			verbose;
	bool			ping;
	uint8_t			speedup;
	enum e_scan		scan_type;
	uint16_t		*ports;
	uint32_t		ports_size;
	char			**ip;
}					t_params;

typedef struct		s_response
{
	char			*service;
	t_scan_status   state;
}					t_response;


typedef struct		s_nmap
{
	bool			verbose;
	char			*dns;
	char			*address;
	uint16_t		dport;
	uint16_t		sport;
	enum e_scan		scan_type;
	t_thread_status	status;
	t_response		*response;
	size_t			index;
}                   t_nmap;

typedef struct		s_pseudo_hdr
{
    uint32_t        s_addr;
    uint32_t        d_addr;
    uint8_t         res;
    uint8_t         protocol;
    uint16_t        length;
}                   t_pseudo_hdr;

// main functions
t_options	*parsing(int ac, char **av);
int			get_nb_scan(enum e_scan scan_type);
uint16_t	*check_ports(char *str);
uint32_t	get_ports_size(uint16_t *ports);
bool		is_ip_addr(char *addr);
void		thread_exec(t_nmap *job);
void		scan_port(t_nmap *job);
bool		init_threads(int nb_job, t_params *params, pthread_t *threads, t_nmap *nmap);
bool		join_threads(uint8_t speedup, int nb_job, pthread_t *threads);
uint16_t	checksum(void *b, int len);
uint16_t	tcp_checksum(struct iphdr *ip, struct tcphdr *tcp);
char		*get_service_name(enum e_scan scan_type, uint16_t port);
uint32_t	get_my_ip_addr(void);
struct sockaddr_in	get_sock_addr(t_nmap *job);
uint64_t	get_time_now(void);
t_params	*make_ping(t_params *params);
uint32_t	get_ip_from_str(char *addr);

// debug functions
//static void	test(t_nmap nmap);
void		debug_options(t_options opt);
void		debug_options_check(t_options_check opt);
void		debug_addrinfo(struct addrinfo *addr);
void		debug_sockaddr(struct sockaddr_in *addr);
void		debug_ip(struct iphdr *ip);
void		debug_icmp(struct icmphdr *icmp);
void		debug_udp(struct udphdr *udp);
void		debug_network_address(uint32_t ip);
void		debug_params(t_params params);
void		debug_nmaps(t_nmap *nmap);
void		debug_nmap(t_nmap nmap);
void		debug_tcp(struct tcphdr *tcp);

t_nmap      *process(t_params *params);

#endif
