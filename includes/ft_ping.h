#ifndef FT_PING_H
#define FT_PING_H

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <sys/time.h>
#include <netinet/ip_icmp.h>
#include <unistd.h>
#include <errno.h>
#include <limits.h>
#include <math.h>

#ifdef __MAC__
	#include "icmp.h"
#else
	#include <signal.h>
#endif

#include "ft_nmap.h"
#include "utils.h"

#define VERBOSE 1
#define HELP 2
#define IP 4

#define PKT_SIZE 64
#define TTL 255
#define RTT 1

#define IPV4 4
#define MUTE 0
#define NB_PKT_MAX 0

typedef enum	e_bool
{
	FALSE,
	TRUE
}				t_bool;

typedef struct	s_args
{
	/* options for bonus */
	char			quiet;
	char			verbose;
	char			help;
	char			*addr;
	int				rtt;
	int				ttl_max;
	int				count;
}				t_args;

typedef struct	s_time
{
	struct timeval		tv;
	double				curr;
	double				min;
	double				max;
	double				average;
	double				stddev;
}				t_time;

typedef struct	s_pkt
{
//	struct iphdr	ip;
	struct icmphdr	icmp;
	char			msg[PKT_SIZE - sizeof(struct icmphdr)];
}				t_pkt;

typedef struct	s_ping
{
	t_bool				sending;
	int					fd_socket;
	unsigned int		ttl;
	unsigned int		packetSize;
	unsigned int		nbPkt;
	unsigned int		pktLost;
	char				*host;
	unsigned int		host_ul;
	struct timeval		startTime;
	struct s_time		rtt;
	struct s_args		args;
	struct addrinfo		*serverInfo;
}				t_ping;

// main functions
int				ft_ping(t_args args);
unsigned long	convertTimevalToUL(struct timeval time);
unsigned long	get_time_now(void);
unsigned long	timeval_to_ul(struct timeval tv);
void			recvPkt(void);
t_bool			sendPkt(void);
void			*codePingPacket(void);
t_bool			decodePingPacket(char **pkt);
t_time			update_rtt_stat(void);
void			print_not_received_pkt(int duration);
int				check_iphdr(struct iphdr ip);
int				check_icmphdr(struct icmphdr icmp);
struct icmphdr	*align_packet(struct icmphdr *icmp);
void			clean_memory(void);
void			ft_usleep(unsigned int duration);
struct addrinfo	*setServerInfo(char *addr);


// utils
char			**ft_strsplit(char const *s, char c);
char			*ft_strsub(char const *s, unsigned int start, size_t len);
void			*ft_memalloc(size_t size);
void			*ft_memset(void *s, int c, size_t n);
int				ft_strcmp(char const *s1, char const *s2);
unsigned int	ft_count_words(char **tab);
int				ft_atoi(char const *str);
void			ft_strdel(char **as);
int				ft_isdigit(int c);
int				ft_isblank(char c);
int				str_is_number(char *str);
int				tab_2d_len(char **tab);
void			ptr2Ddel(char **tab);
int				ft_isalpha(int c);
size_t			ft_strlen(char const *s);
char			*ft_strdup(char const *str);
int				array_len(char **tab);
void			*ft_memcpy(void *dest, void const *src, size_t n);
char			*ft_strchr(const char *str, int c);
void			*ft_memchr(void const *s, int c, size_t n);


// debug
void	print_ping(t_ping *ping);
void	print_rtt(t_time t);
void	print_msghdr(struct msghdr msg);
void	print_pkt(t_pkt pkt);
void	print_icmp(struct icmphdr icmp);
void	print_ip(struct iphdr ip);
void	print_addrinfo(struct addrinfo addr);

#endif
